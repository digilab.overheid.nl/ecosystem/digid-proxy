module gitlab.com/digilab.overheid.nl/ecosystem/digid-proxy

go 1.22.1

require (
	github.com/crewjam/saml v0.4.14
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/beevik/etree v1.4.0 // indirect
	github.com/crewjam/httperr v0.2.0 // indirect
	github.com/golang-jwt/jwt/v4 v4.5.0 // indirect
	github.com/jonboulle/clockwork v0.4.0 // indirect
	github.com/mattermost/xml-roundtrip-validator v0.1.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/russellhaering/goxmldsig v1.4.0 // indirect
	golang.org/x/crypto v0.23.0 // indirect
)
