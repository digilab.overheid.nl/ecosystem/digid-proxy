# Stage 1
FROM golang:1.22.2-alpine3.19 as builder

# Copy code into the container. Note: copy to a dir instead of `.`, since $GOPATH may not contain a go.mod file
WORKDIR /build

COPY . .

# Build the Go Files
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-s -w" -o server .

# Stage 2
FROM alpine

# # Add timezones
# RUN apk add --no-cache tzdata

# Copy the bin from builder to root.
COPY --from=builder /build/server /

# Copy the dirs with static files
COPY assets /assets
COPY .well-known /.well-known

EXPOSE 8080

ENTRYPOINT ["./server"]
