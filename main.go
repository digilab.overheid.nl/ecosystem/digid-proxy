package main

import (
	"bytes"
	"context"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/beevik/etree"
	"github.com/crewjam/saml"
	"github.com/crewjam/saml/samlsp"
	"gopkg.in/yaml.v3"
)

// Config represents the entire YAML configuration
type Config struct {
	ProxyCookieDomain string                  `yaml:"proxyCookieDomain"`
	Clients           map[string]ConfigClient `yaml:"clients"`
}

type ConfigClient struct {
	RedirectURL      string `yaml:"redirectUrl"`
	App2appReturnURL string `yaml:"app2appReturnUrl"`
}

var config Config

// main starts a server on port 8080. Usage: DIGID_CERTIFICATE_PATH=logiuscert.cer DIGID_PRIVATE_KEY_PATH=privkey.key DIGID_CA_CERTIFICATE_PATH=QuoVadis_PKIoverheid_Private_Services_CA-G1.cer APP_CLIENTS_CONFIG_PATH=config.example.yaml go run .
func main() {
	// Read the client config from the yaml config file
	data, err := os.ReadFile(os.Getenv("APP_CLIENTS_CONFIG_PATH"))
	if err != nil {
		fmt.Printf("error reading config file: %v\n", err)
		return
	}

	err = yaml.Unmarshal(data, &config)
	if err != nil {
		fmt.Printf("error unmarshaling config data: %v\n", err)
		return
	}

	// Load the key pair
	keyPair, err := tls.LoadX509KeyPair(
		os.Getenv("DIGID_CERTIFICATE_PATH"), // Certificate provided by Logius after signup
		os.Getenv("DIGID_PRIVATE_KEY_PATH"), // Private key that belongs to the public key sent to Logius during the application process
	)
	if err != nil {
		fmt.Println("error reading key pair:", err)
		return
	}

	keyPair.Leaf, err = x509.ParseCertificate(keyPair.Certificate[0]) // IMPROVE: make sure [0] exists? Or would the function above otherwise return an error?
	if err != nil {
		fmt.Println("error parsing key pair certificate:", err)
		return
	}

	// Load CA certificate
	caCertContent, err := os.ReadFile(
		os.Getenv("DIGID_CA_CERTIFICATE_PATH"), // Source: https://www.pkioverheid.nl/ => QuoVadis PKIoverheid Private Services CA - G1
	)
	if err != nil {
		fmt.Println("error reading CA certificate:", err)
		return
	}

	caCert, err := x509.ParseCertificate(caCertContent)
	if err != nil {
		fmt.Println("error parsing CA certificate:", err)
		return
	}

	caCertPool := x509.NewCertPool()
	caCertPool.AddCert(caCert)

	// Setup HTTPS client
	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{keyPair}, // Note: not required for metadata request
		RootCAs:      caCertPool,
	}
	transport := &http.Transport{TLSClientConfig: tlsConfig}
	client := &http.Client{Transport: transport}

	// Fetch the metadata
	idpMetadataURL, _ := url.Parse("https://was-preprod1.digid.nl/saml/idp/metadata")

	idpMetadata, err := samlsp.FetchMetadata(context.Background(), client,
		*idpMetadataURL)
	if err != nil {
		fmt.Println("error fetching metadata:", err)
		return
	}

	rootURL, _ := url.Parse("https://saml-preprod.digilab.network")

	// Common options for login and logout, see https://www.logius.nl/domeinen/toegang/digid/documentatie/koppelvlakspecificatie-digid-saml-authenticatie#index-42
	opts := samlsp.Options{
		URL:         *rootURL,
		Key:         keyPair.PrivateKey.(*rsa.PrivateKey),
		Certificate: keyPair.Leaf,
		HTTPClient:  client,
		EntityID:    "https://saml-preprod.digilab.network",
		ForceAuthn:  false,
		IDPMetadata: idpMetadata,
		RelayStateFunc: func(w http.ResponseWriter, r *http.Request) string {
			// Return the state from the ref GET parameter
			return r.URL.Query().Get("state")
		},
		CookieName:     defaultSessionCookieName,
		CookieSameSite: http.SameSiteLaxMode,
	}

	logoutMiddleware, _ := samlsp.New(opts)

	// Extra options for login, see https://www.logius.nl/domeinen/toegang/digid/documentatie/koppelvlakspecificatie-digid-saml-authenticatie#index-16
	loginOpts := opts
	loginOpts.UseArtifactResponse = true
	loginOpts.RequestedAuthnContext = &saml.RequestedAuthnContext{
		Comparison:           "minimum",
		AuthnContextClassRef: "urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport",
	}
	loginOpts.DefaultRedirectURI = "https://saml-preprod.digilab.network/saml/sp/artifact_resolution"
	loginOpts.SignRequest = true
	loginMiddleware, _ := samlsp.New(loginOpts)

	// Overwrite the SessionProvider with our own custom provider, since we want to set a different cookie domain
	csp := CookieSessionProvider{
		Name:     opts.CookieName,
		MaxAge:   time.Hour,
		HTTPOnly: true,
		Secure:   opts.URL.Scheme == "https",
		SameSite: opts.CookieSameSite,
		Codec:    samlsp.DefaultSessionCodec(opts),
	}
	loginMiddleware.Session = samlsp.SessionProvider(csp)
	logoutMiddleware.Session = samlsp.SessionProvider(csp)

	// Define routes
	http.Handle("/login", loginMiddleware.RequireAccount(http.HandlerFunc(login)))
	http.Handle("/saml/", loginMiddleware) // For handling requests to the redirect URL, etc.
	http.Handle("/logout", logout(logoutMiddleware))
	http.Handle("/app2app-request", app2appRequest(&loginMiddleware.ServiceProvider))
	http.HandleFunc("/app2app", app2AppFallback)
	http.HandleFunc("/app2app-artifact", checkApp2AppArtifact(&loginMiddleware.ServiceProvider))

	// Static files
	http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("./assets"))))
	http.Handle("/.well-known/", http.StripPrefix("/.well-known/", http.FileServer(http.Dir("./.well-known"))))

	fmt.Println("starting server on :8080")
	http.ListenAndServe(":8080", nil)
}

// Login handler
func login(w http.ResponseWriter, r *http.Request) {
	// Parse and decode the state query parameter
	state := r.URL.Query().Get("state")

	// Get the redirect URL from the config
	cfg, found := config.Clients[state]
	if !found {
		fmt.Fprintf(w, "client '%s': not found in config", state)
		return
	}

	// Get the cookie in which the token was set
	cookie, err := r.Cookie(defaultSessionCookieName)
	if err != nil {
		fmt.Println("cookie not found:", err)
		return
	}

	// Parse the redirect URL
	redirectURL, err := url.Parse(cfg.RedirectURL)
	if err != nil {
		fmt.Println("error parsing redirect URL:", err)
		return
	}

	// Retrieve the existing query parameters
	queryParams := redirectURL.Query()

	// Add the new query parameter
	queryParams.Add("token", cookie.Value)

	// Encode the updated query parameters back into the URL
	redirectURL.RawQuery = queryParams.Encode()

	// Redirect to the specified redirect URL
	http.Redirect(w, r, redirectURL.String(), http.StatusFound)
}

// app2appRequest returns a JSON object that the app should send using a deep link to the DigiD app. See https://www.logius.nl/domeinen/toegang/digid/documentatie/functionele-beschrijving-digid-app#index-23 and https://www.logius.nl/domeinen/toegang/digid/documentatie/koppelvlakspecificatie-digid-saml-authenticatie#index-54 for more information
func app2appRequest(sp *saml.ServiceProvider) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Enable CORS for this route, so the URL can be fetched from Flutter using a browser
		w.Header().Set("Access-Control-Allow-Origin", "*")

		// Get a SAML authentication request. Note: using an empty relay state for simplicity
		authReq, err := sp.MakeRedirectAuthenticationRequest("")
		if err != nil {
			fmt.Println("error getting authentication request for app2app:", err)
			return
		}

		resp := struct {
			Icon        string
			ReturnUrl   string
			SAMLRequest string
			Host        string
			SigAlg      string
			Signature   string
			// Note: RelayState can be added, see the links above
		}{
			"https://saml-preprod.digilab.network/assets/icon.png", // Note: cannot be SVG, should have a sufficiently high resolution (e.g. 256x256 px)
			"https://saml-preprod.digilab.network/app2app",         // TODO: use from the config if allowed to set arbitrary return URLs
			authReq.Query().Get("SAMLRequest"),
			"preprod1.digid.nl", // Note: leave empty on production
			authReq.Query().Get("SigAlg"),
			authReq.Query().Get("Signature"),
		}

		respJSON, err := json.Marshal(resp)
		if err != nil {
			fmt.Println("error encoding app2app request data:", err)
			return
		}

		// Set a response header and write the response
		w.Header().Set("Content-Type", "application/json")
		w.Write(respJSON)
	}
}

// app2AppFallback returns a fallback message. After login, the DigiD app sends the user to this URL, which is intended to be opened as deep link (Android App Link or iOS Universal Link) in the mobile client app. This is a fallback in case the link is opened in the browser instead
func app2AppFallback(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Deze URL hoort te worden geopend in de app. Zorg ervoor dat de app is geïnstalleerd en dat is toegestaan om links te openen in de app.")
}

// checkApp2AppArtifact handles an app2app SAML artifact. The mobile app sends a request to this endpoint to decode the SAML artifact that is included in the deep link
func checkApp2AppArtifact(sp *saml.ServiceProvider) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Get the SAMLart query parameter
		artifactID := r.URL.Query().Get("SAMLart")

		// Return as JSON response, containing either the subject
		var resp struct {
			NameID string `json:",omitempty"`
			Error  string `json:",omitempty"`
		}

		ctx := context.Background()

		assertion, err := resolveSAMLArtifact(ctx, artifactID, sp)
		if err == nil {
			resp.NameID = assertion.Subject.NameID.Value
		} else {
			resp.Error = err.Error()
		}

		// Return the response as JSON
		respJSON, err := json.Marshal(resp)
		if err != nil {
			fmt.Println("error encoding app2app artifact response:", err)
			return
		}

		// Set a response header and write the response
		w.Header().Set("Content-Type", "application/json")
		w.Write(respJSON)
	}
}

// resolveSAMLArtifact resolves the artifact with the specified ID and returns its assertion
func resolveSAMLArtifact(ctx context.Context, artifactID string, sp *saml.ServiceProvider) (*saml.Assertion, error) {
	// Generate an artifact resolve request
	artifactResolveRequest, err := sp.MakeArtifactResolveRequest(artifactID)
	if err != nil {
		return nil, fmt.Errorf("cannot generate artifact resolution request: %w", err)
	}

	requestBody, err := elementToBytes(artifactResolveRequest.SoapRequest())
	if err != nil {
		return nil, err
	}

	// Do the request
	req, err := http.NewRequestWithContext(ctx, "POST", sp.GetArtifactBindingLocation(saml.SOAPBinding),
		bytes.NewReader(requestBody))
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "text/xml")

	response, err := sp.HTTPClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("cannot resolve artifact: %w", err)
	}

	defer func() {
		if err := response.Body.Close(); err != nil {
			fmt.Printf("error while closing response body during artifact resolution: %s", err)
		}
	}()

	if response.StatusCode != 200 {
		return nil, fmt.Errorf("error during artifact resolution: HTTP status %d (%s)", response.StatusCode, response.Status)
	}

	responseBody, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, fmt.Errorf("error during artifact resolution: %s", err)
	}

	sp.AllowIDPInitiated = true // Avoid checking the 'possibleRequestIDs'

	// Validate and decode the response
	assertion, err := sp.ParseXMLArtifactResponse(responseBody, nil, artifactResolveRequest.ID)
	if err != nil {
		if ivr, ok := err.(*saml.InvalidResponseError); ok {
			err = ivr.PrivateErr
		}
	}

	return assertion, err
}

// Logout handler
func logout(logoutMiddleware *samlsp.Middleware) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// // Only for DigiD EI (Eenmalig Inloggen): generate a logout URL

		// // Get the nameID of the user that is logged in
		// nameID := samlsp.AttributeFromContext(r.Context(), "urn:oasis:names:tc:SAML:attribute:subject-id")

		// // Generate the logout request
		// logoutMiddleware.ServiceProvider.SignatureMethod = ""
		// logoutReq, err := logoutMiddleware.ServiceProvider.MakeRedirectLogoutRequest(nameID, "")
		// if err != nil {
		// 	fmt.Fprintf(w, "error generating logout request: %v", err)
		// 	return
		// }

		// // Sign the logout request with the code below, since the crewjam/saml library does not do this as DigiD expects (SAMLRequest too long, no SigAlg and Signature parameters included). Also, DigiD has a limit on the URL / query parameter length, but this can be worked around using the HTTPPostBinding instead of a redirect
		// q := logoutReq.Query()
		// logoutMiddleware.ServiceProvider.SignatureMethod = dsig.RSASHA1SignatureMethod

		// q.Add("SigAlg", logoutMiddleware.ServiceProvider.SignatureMethod)
		// logoutReq.RawQuery = q.Encode()

		// signingContext, err := saml.GetSigningContext(&logoutMiddleware.ServiceProvider)
		// if err != nil {
		// 	fmt.Fprintf(w, "error signing session: %v", err)
		// 	return
		// }

		// sig, err := signingContext.SignString(logoutReq.String())
		// if err != nil {
		// 	fmt.Fprintf(w, "error signing logout URL: %v", err)
		// 	return
		// }

		// q.Add("Signature", base64.StdEncoding.EncodeToString(sig))
		// logoutReq.RawQuery = q.Encode()

		// Delete the session/cookie
		if err := logoutMiddleware.Session.DeleteSession(w, r); err != nil {
			fmt.Fprintf(w, "error deleting session: %v", err)
			return
		}

		// // Only for DigiD EI (Eenmalig Inloggen): redirect to the DigiD logout endpoint
		// http.Redirect(w, r, logoutReq.String(), http.StatusFound)

		// Return a simple response. IMPROVE: redirect to an endpoint specified for the client in the config
		fmt.Fprintln(w, "Uitgelogd")
	}
}

func elementToBytes(el *etree.Element) ([]byte, error) {
	namespaces := map[string]string{}
	for _, childEl := range el.FindElements("//*") {
		ns := childEl.NamespaceURI()
		if ns != "" {
			namespaces[childEl.Space] = ns
		}
	}

	doc := etree.NewDocument()
	doc.SetRoot(el.Copy())
	for space, uri := range namespaces {
		doc.Root().CreateAttr("xmlns:"+space, uri)
	}

	return doc.WriteToBytes()
}
